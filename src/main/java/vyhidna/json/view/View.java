package vyhidna.json.view;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vyhidna.json.controller.CheckSchema;
import vyhidna.json.model.Card;

public class View {

  Logger LOG = LogManager.getLogger(CheckSchema.class);
  Scanner scanner = new Scanner(System.in);

  public int choosePerser() {
    LOG.info("If you want work with Gson enter 1, if with Jackson 2");
    return scanner.nextInt();
  }

  public int readOrWrite(){
    LOG.info("If you want read enter 1, if write 2");
    return scanner.nextInt();
  }

  public int toContinue(){
    LOG.info("If you want to continue enter 1, else enter 0");
    return scanner.nextInt();
  }

  public void wrongParserNumber(){
    LOG.info("You entered wrong number choosing parser");
  }

  public void wrongReadOrWriteNumber(){
    LOG.info("You entered wrong number choosing read or write operation");
  }

  public void showCards(Card[] cards){
    for (Card card : cards) {
      LOG.info(card);
    }
  }

}
