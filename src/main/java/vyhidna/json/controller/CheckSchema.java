package vyhidna.json.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

public class CheckSchema {
  Logger LOG = LogManager.getLogger(CheckSchema.class);

  public boolean checkSchema(String schemaPath, String jsonPath) throws FileNotFoundException {
    try {
      JSONObject jsonSchema = new JSONObject(
          new JSONTokener(new FileReader(schemaPath)));
      JSONObject jsonSubject = new JSONObject(
          new JSONTokener(new FileReader(jsonPath)));
      Schema schema = SchemaLoader.load(jsonSchema);
      schema.validate(jsonSubject);
    }catch (ValidationException validationE){
      LOG.error(validationE.getMessage());
      return false;
    }
    return true;
  }


}
