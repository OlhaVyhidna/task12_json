package vyhidna.json.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import javax.swing.text.html.parser.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vyhidna.json.model.Card;
import vyhidna.json.model.CardComparator;
import vyhidna.json.model.OldCards;
import vyhidna.json.util.GsonParser;
import vyhidna.json.util.JacksonParser;
import vyhidna.json.view.View;

public class ParserController {

  String schemaPath = "src/main/resources/card/cardSchema.json";
  String jsonPath = "src/main/resources/card/card.json";
  Logger LOG = LogManager.getLogger(CheckSchema.class);
  View view;
  CardComparator cardComparator;
  GsonParser gsonParser;
  JacksonParser jacksonParser;
  Card[] cards;

  public ParserController() {
    this.view = new View();
    cardComparator = new CardComparator();
    gsonParser = new GsonParser();
    jacksonParser = new JacksonParser();
  }

  public void start() throws IOException {
    menu();
    int toContinue = view.toContinue();
    if (toContinue==1){
      menu();
    }
  }

  private void menu() throws IOException {
    int parser = view.choosePerser();
    Card [] cards =null;
    if (parser == 1) {
      int decision = view.readOrWrite();
      if (decision==1){
       cards = redByGson().getCards();
      }else if (decision==2){
        if (cards==null){
          cards = redByGson().getCards();
          writeByGson(cards);
        }
      }else{
        view.wrongReadOrWriteNumber();
      }
    } else if (parser == 2) {
      int decision = view.readOrWrite();
      if (decision==1){
        cards = readByJackson().getCards();
      }else if (decision==2){
        if (cards==null){
          cards=readByJackson().getCards();
        }
        writeByJackson(cards);
      }else {
        view.wrongReadOrWriteNumber();
      }

    } else {
      view.wrongParserNumber();
    }
  }


  private OldCards redByGson() throws FileNotFoundException {
    OldCards oldCards = gsonParser.parseByGson(jsonPath, schemaPath);
    view.showCards(oldCards.getCards());
    return oldCards;
  }

  private void writeByGson(Card[] cards) {
    Arrays.sort(cards, cardComparator);
    gsonParser.toJsone(cards);
  }

  private OldCards readByJackson() throws IOException {
    OldCards oldCards = jacksonParser.parseToObject(jsonPath);
    view.showCards(oldCards.getCards());
    return oldCards;
  }

  private void writeByJackson(Card[] cards) throws IOException {
    Arrays.sort(cards);
    jacksonParser.parseIntoJson(cards);
  }
}
