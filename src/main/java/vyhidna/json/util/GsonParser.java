package vyhidna.json.util;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import vyhidna.json.controller.CheckSchema;
import vyhidna.json.model.Card;
import vyhidna.json.model.OldCards;

public class GsonParser {

  public OldCards parseByGson(String jsonPath, String schemaPath) throws FileNotFoundException {
    OldCards oldCards = null;
    if (new CheckSchema().checkSchema(schemaPath, jsonPath)) {
      Gson gson = new Gson();
      try (Reader reader = new FileReader(jsonPath)) {
        oldCards = gson.fromJson(reader, OldCards.class);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return oldCards;
  }

  public void toJsone(Card[] cards) {
    Gson gson = new Gson();
    for (int i = 0; i < cards.length; i++) {
      String s = gson.toJson(cards[i]);
      System.out.println(s);
    }
  }


}
