package vyhidna.json.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import vyhidna.json.model.Card;
import vyhidna.json.model.CardComparator;
import vyhidna.json.model.OldCards;

public class JacksonParser {

  public OldCards parseToObject(String jsonPath) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    OldCards oldCards = objectMapper.readValue(new File(jsonPath), OldCards.class);
    return oldCards;
  }

  public void parseIntoJson(Card[] cards) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    Arrays.sort(cards, new CardComparator());
      objectMapper.writeValue(new File("src/main/resources/card/writeJson.json"), cards);
  }

}
