package vyhidna.json.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//  @XmlRootElement(name = "OldCards")
//  @XmlAccessorType(XmlAccessType.FIELD)
  public class OldCards {

//    @XmlElement(name = "card")
    private Card [] cards;

    public Card[] getCards() {
      return cards;
    }

    public void setCards(Card[] cards) {
      this.cards = cards;
    }
  }


